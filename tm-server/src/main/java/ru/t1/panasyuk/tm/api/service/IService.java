package ru.t1.panasyuk.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.enumerated.Sort;
import ru.t1.panasyuk.tm.dto.model.AbstractModelDTO;

import java.util.List;

public interface IService<M extends AbstractModelDTO> {

    @Nullable
    List<M> findAll(@Nullable Sort sort) throws Exception;

}