package ru.t1.panasyuk.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.api.repository.IUserRepository;
import ru.t1.panasyuk.tm.api.service.IConnectionService;
import ru.t1.panasyuk.tm.api.service.IProjectTaskService;
import ru.t1.panasyuk.tm.api.service.IPropertyService;
import ru.t1.panasyuk.tm.api.service.IUserService;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.exception.entity.EntityNotFoundException;
import ru.t1.panasyuk.tm.exception.entity.UserNotFoundException;
import ru.t1.panasyuk.tm.exception.field.*;
import ru.t1.panasyuk.tm.dto.model.UserDTO;
import ru.t1.panasyuk.tm.util.HashUtil;

import java.util.Collection;
import java.util.List;

public final class UserService implements IUserService {

    @NotNull
    private final IConnectionService connectionService;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final IProjectTaskService projectTaskService;

    public UserService(
            @NotNull final IPropertyService propertyService,
            @NotNull final IConnectionService connectionService,
            @NotNull final IProjectTaskService projectTaskService
    ) {
        this.connectionService = connectionService;
        this.projectTaskService = projectTaskService;
        this.propertyService = propertyService;
    }

    public UserDTO add(@Nullable final UserDTO user) {
        if (user == null) return null;
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            try {
                @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
                repository.add(user);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return user;
    }

    @NotNull
    @Override
    public UserDTO create(@NotNull final String login, @NotNull final String password) throws Exception {
        if (login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginExistsException();
        if (password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        return add(user);
    }

    @NotNull
    @Override
    public UserDTO create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final String email
    ) throws Exception {
        if (login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginExistsException();
        if (password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExist(email)) throw new EmailExistsException();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        user.setRole(Role.USUAL);
        return add(user);
    }

    @NotNull
    @Override
    public UserDTO create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final String email,
            @Nullable final Role role
    ) throws Exception {
        if (login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginExistsException();
        if (password.isEmpty()) throw new PasswordEmptyException();
        if (email != null && !email.isEmpty() && isEmailExist(email)) throw new EmailExistsException();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        if (role == null) user.setRole(Role.USUAL);
        else user.setRole(role);
        return add(user);
    }

    public void clear() {
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            try {
                @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
                repository.clear();
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        boolean result;
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            try {
                @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
                result = repository.findOneById(id) != null;
            } catch (@NotNull final Exception e) {
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return result;
    }

    @Nullable
    @Override
    public List<UserDTO> findAll() {
        @Nullable final List<UserDTO> models;
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            try {
                @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
                models = repository.findAll();
            } catch (@NotNull final Exception e) {
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return models;
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable UserDTO user;
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            try {
                @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
                user = repository.findByLogin(login);
            } catch (@NotNull final Exception e) {
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return user;
    }

    @Nullable
    @Override
    public UserDTO findByEmail(@Nullable final String email) throws Exception {
        @Nullable UserDTO user;
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            try {
                @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
                user = repository.findByEmail(email);
            } catch (@NotNull final Exception e) {
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return user;
    }

    @Nullable
    @Override
    public UserDTO findOneById(@Nullable final String id) {
        if (id == null) return null;
        @Nullable final UserDTO user;
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            try {
                @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
                user = repository.findOneById(id);
            } catch (@NotNull final Exception e) {
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return user;
    }

    @Nullable
    @Override
    public UserDTO findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index > getSize()) throw new IndexIncorrectException();
        @Nullable final UserDTO user;
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            try {
                @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
                user = repository.findOneByIndex(index);
            } catch (@NotNull final Exception e) {
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return user;
    }

    @Override
    public int getSize() {
        int result;
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            try {
                @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
                result = repository.getSize();
            } catch (@NotNull final Exception e) {
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return result;
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        update(user);
    }

    @NotNull
    @Override
    public UserDTO removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final UserDTO removedUser;
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            try {
                @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
                removedUser = repository.findOneById(id);
                if (removedUser == null) throw new EntityNotFoundException();
                repository.remove(removedUser);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return removedUser;
    }

    @NotNull
    @Override
    public UserDTO removeByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index > getSize()) throw new IndexIncorrectException();
        @Nullable final UserDTO removedUser;
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            try {
                @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
                removedUser = repository.findOneByIndex(index);
                if (removedUser == null) throw new EntityNotFoundException();
                repository.remove(removedUser);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return removedUser;
    }

    @NotNull
    @Override
    public UserDTO removeByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return remove(user);
    }

    @NotNull
    @Override
    public UserDTO remove(@Nullable final UserDTO user) throws Exception {
        if (user == null) throw new EntityNotFoundException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            try {
                @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
                projectTaskService.clearProjects(user.getId());
                repository.remove(user);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return user;
    }

    @NotNull
    @Override
    public UserDTO removeByEmail(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final UserDTO user = findByEmail(email);
        if (user == null) throw new UserNotFoundException();
        return remove(user);
    }

    @NotNull
    @Override
    public UserDTO setPassword(@Nullable final String id, @Nullable final String password) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final UserDTO user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        update(user);
        return user;
    }

    @NotNull
    @Override
    public Collection<UserDTO> set(@NotNull final Collection<UserDTO> users) {
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            try {
                @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
                repository.clear();
                for (@NotNull final UserDTO user : users)
                    repository.add(user);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return users;
    }

    @Override
    public void update(@NotNull final UserDTO user) {
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            try {
                @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
                repository.update(user);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        update(user);
    }

    @NotNull
    @Override
    public UserDTO updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final UserDTO user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        update(user);
        return user;
    }

}