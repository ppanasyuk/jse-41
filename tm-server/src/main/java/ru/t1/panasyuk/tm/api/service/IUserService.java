package ru.t1.panasyuk.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.dto.model.UserDTO;

import java.util.Collection;
import java.util.List;

public interface IUserService {

    UserDTO add(@Nullable UserDTO user);

    void clear();

    @NotNull
    UserDTO create(@NotNull String login, @NotNull String password) throws Exception;

    @NotNull
    UserDTO create(@NotNull String login, @NotNull String password, @Nullable String email) throws Exception;

    @NotNull
    UserDTO create(@NotNull String login, @NotNull String password, @Nullable String email, @Nullable Role role) throws Exception;

    boolean existsById(@Nullable String id);

    List<UserDTO> findAll();

    UserDTO findByLogin(@Nullable String login) throws Exception;

    UserDTO findByEmail(@Nullable String email) throws Exception;

    UserDTO findOneById(@Nullable String id);

    UserDTO findOneByIndex(@Nullable Integer index);

    int getSize();

    Boolean isLoginExist(@Nullable String login) throws Exception;

    Boolean isEmailExist(@Nullable String email) throws Exception;

    UserDTO removeById(@Nullable String id);

    void lockUserByLogin(@Nullable String login) throws Exception;

    UserDTO remove(@Nullable UserDTO user) throws Exception;

    UserDTO removeByIndex(@Nullable Integer index);

    @NotNull
    UserDTO removeByLogin(@Nullable String login) throws Exception;

    @NotNull
    UserDTO removeByEmail(@Nullable String email) throws Exception;

    Collection<UserDTO> set(@NotNull Collection<UserDTO> users);

    @NotNull
    UserDTO setPassword(@Nullable String id, @Nullable String password) throws Exception;

    void unlockUserByLogin(@Nullable String login) throws Exception;

    void update(@NotNull UserDTO user);

    @NotNull
    UserDTO updateUser(@Nullable String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName);

}