package ru.t1.panasyuk.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.dto.model.SessionDTO;

import java.util.List;

public interface ISessionService extends IUserOwnedService<SessionDTO> {

    SessionDTO remove(@Nullable SessionDTO session);

    List<SessionDTO> findAll();

}
