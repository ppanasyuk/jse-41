package ru.t1.panasyuk.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.api.repository.IProjectRepository;
import ru.t1.panasyuk.tm.api.repository.ITaskRepository;
import ru.t1.panasyuk.tm.api.service.IConnectionService;
import ru.t1.panasyuk.tm.api.service.IProjectTaskService;
import ru.t1.panasyuk.tm.exception.entity.ProjectNotFoundException;
import ru.t1.panasyuk.tm.exception.entity.TaskNotFoundException;
import ru.t1.panasyuk.tm.exception.field.IndexIncorrectException;
import ru.t1.panasyuk.tm.exception.field.ProjectIdEmptyException;
import ru.t1.panasyuk.tm.exception.field.TaskIdEmptyException;
import ru.t1.panasyuk.tm.dto.model.ProjectDTO;
import ru.t1.panasyuk.tm.dto.model.TaskDTO;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    final IConnectionService connectionService;

    public ProjectTaskService(
            @NotNull final IConnectionService connectionService
    ) {
        this.connectionService = connectionService;
    }

    @NotNull
    @Override
    public TaskDTO bindTaskToProject(
            @NotNull final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final TaskDTO task;
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            try {
                @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
                @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
                boolean isExist = projectRepository.findOneById(userId, projectId) != null;
                if (!isExist) throw new ProjectNotFoundException();
                task = taskRepository.findOneById(userId, taskId);
                if (task == null) throw new TaskNotFoundException();
                task.setProjectId(projectId);
                taskRepository.update(task);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return task;
    }

    @Override
    public ProjectDTO removeProjectById(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @Nullable ProjectDTO project;
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            try {
                @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
                @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
                project = projectRepository.findOneById(userId, projectId);
                if (project == null) throw new ProjectNotFoundException();
                taskRepository.removeAllByProjectId(userId, projectId);
                projectRepository.remove(project);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return project;
    }

    @Override
    public ProjectDTO removeProjectByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        @Nullable final ProjectDTO project;
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            try {
                @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
                if (index > projectRepository.getSize(userId)) throw new IndexIncorrectException();
                project = projectRepository.findOneByIndex(userId, index);
                if (project == null) throw new ProjectNotFoundException();
                removeProjectById(userId, project.getId());
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return project;
    }

    @Override
    public void clearProjects(@NotNull final String userId) {
        @Nullable final List<ProjectDTO> projects;
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            try {
                @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
                projects = projectRepository.findAllForUser(userId);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        if (projects == null) return;
        for (@NotNull final ProjectDTO project : projects) removeProjectById(userId, project.getId());
    }

    @NotNull
    @Override
    public TaskDTO unbindTaskFromProject(
            @NotNull final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final TaskDTO task;
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            try {
                @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
                @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
                boolean isExist = projectRepository.findOneById(userId, projectId) != null;
                if (!isExist) throw new ProjectNotFoundException();
                task = taskRepository.findOneById(userId, taskId);
                if (task == null) throw new TaskNotFoundException();
                task.setProjectId(null);
                taskRepository.update(task);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return task;
    }

}