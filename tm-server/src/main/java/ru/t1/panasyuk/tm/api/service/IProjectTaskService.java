package ru.t1.panasyuk.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.dto.model.ProjectDTO;
import ru.t1.panasyuk.tm.dto.model.TaskDTO;

public interface IProjectTaskService {

    @NotNull
    TaskDTO bindTaskToProject(@NotNull String userId, @Nullable String projectId, @Nullable String taskId) throws Exception;

    ProjectDTO removeProjectById(@NotNull String userId, @Nullable String projectId) throws Exception;

    ProjectDTO removeProjectByIndex(@NotNull String userId, @Nullable Integer index) throws Exception;

    void clearProjects(@NotNull String userId) throws Exception;

    @NotNull
    TaskDTO unbindTaskFromProject(@NotNull String userId, @Nullable String projectId, @Nullable String taskId) throws Exception;

}