package ru.t1.panasyuk.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.api.model.IWBS;
import ru.t1.panasyuk.tm.constant.DBConst;
import ru.t1.panasyuk.tm.enumerated.Status;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = DBConst.TABLE_PROJECT)
public final class Project extends AbstractUserOwnedModel implements IWBS {

    private static final long serialVersionUID = 1;

    @NotNull
    @Column(name = DBConst.COLUMN_NAME)
    private String name = "";

    @Nullable
    @Column(name = DBConst.COLUMN_DESCRIPTION)
    private String description = "";

    @Nullable
    @Enumerated(EnumType.STRING)
    @Column(name = DBConst.COLUMN_STATUS, length = 30)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @OneToMany(mappedBy = "project")
    private List<Task> tasks = new ArrayList<>();

    public Project(@NotNull final String name, @NotNull final Status status) {
        this.name = name;
        this.status = status;
    }

    @NotNull
    @Override
    public String toString() {
        return String.format(
                "[Name=%s, Description=%s, Created=%tF %tT, Status=%s]",
                name, description, created, created, Status.toName(status)
        );
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public boolean equals(@NotNull final Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof Project)) return false;
        @NotNull final Project project = (Project) obj;
        return project.getId().equals(this.getId());
    }

}